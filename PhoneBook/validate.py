__author__ = 'msabbota'

import re


def validEmail(email):
    # Objective: validate a proper e-mail address is accepted (does not work against all possible e-mail combinations)
    # Precondition: String with a properly formated e-mail of <user>@<host>.<domain_suffix>
    # Postcondition: Return true if valid --OR-- None if invalid
    if not re.match("[0-9a-z._%+-]+@[0-9a-z._%+-]+$", email):
        return False
    return True

def validName(name):
    # Objective 1: Validate a Name to make sure it's is valid for the Phonebook
    # Precondition: string name
    # Postcondition 1: If String != null, return 1
    # Postcondition 2: If String == null, return 0
    if name == '':
        return False
    else:
        return True

def validPhone(phone):
    # Objective: validate a proper phone number doesn't have invalid chars (non-numeric or -)
    # Precondition: A valid US phone number that doesn't exceed 10 digits (12 including -)
    # Postcondition: Return true if valid --OR-- None if invalid
    if not re.match("[\d+]{7,12}$", phone):
        return False
    return True

# Used for unit testing only
if __name__ == '__main__':
    pass
