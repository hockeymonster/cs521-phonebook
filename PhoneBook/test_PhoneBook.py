__author__ = 'msabbota'

import unittest

from PhoneBook import *
from UserEntry import *


# Test cases for phonebook (to be implemented).
class PhoneBookTest(unittest.TestCase):
    def setUp(self):
        self.testPhoneBook = PhoneBook('./testfile.dat')
        self.user0 = UserEntry("John", "Doe", "june@foo.com", "15551212")
        self.user1 = UserEntry("Flash", "Gordon", "flash@flash.net", "17777777")
        self.user2 = UserEntry("Normal", "User", "a@b.net", "222333444")
        self.user3 = UserEntry("A", "User", "a01@b.net", "015551212")
        self.user4 = UserEntry("B", "User", "a02@c.net", "025551212")
        self.user5 = UserEntry("C", "User", "a03@b.net", "035551212")
        self.user6 = UserEntry("D", "User", "a04@e.net", "045551212")
        self.user7 = UserEntry("E", "User", "a05@f.net", "055551212")
        self.user8 = UserEntry("F", "User", "a06@g.net", "065551212")
        self.user9 = UserEntry("G", "User", "a07@h.net", "075551212")
        self.user10 = UserEntry("H", "User", "a08@i.net", "085551212")
        self.user11 = UserEntry("I", "User", "a09@j.net", "105551212")
        self.user12 = UserEntry("J", "User", "a10@k.net", "115551212")

    def test_PhoneBook(self):
        # Quick validation of user objects
        self.assertEqual("John Doe", self.user0._firstName + " " + self.user0._lastName)
        self.assertEqual("flash@flash.net", self.user1._email)

        # Validate adding things to our phonebook. Lets zero-it out and load it with data
        self.assertTrue(self.testPhoneBook.clearRecords())
        self.assertFalse(self.testPhoneBook.addRecord(list()))
        self.assertTrue(self.testPhoneBook.addRecord(self.user0))
        self.assertTrue(self.testPhoneBook.addRecord(self.user1))
        self.assertTrue(self.testPhoneBook.addRecord(self.user2))
        self.assertTrue(self.testPhoneBook.addRecord(self.user3))
        self.assertTrue(self.testPhoneBook.addRecord(self.user4))
        self.assertTrue(self.testPhoneBook.addRecord(self.user5))
        self.assertTrue(self.testPhoneBook.addRecord(self.user6))
        self.assertTrue(self.testPhoneBook.addRecord(self.user7))
        self.assertTrue(self.testPhoneBook.addRecord(self.user8))
        self.assertTrue(self.testPhoneBook.addRecord(self.user9))
        self.assertTrue(self.testPhoneBook.addRecord(self.user10))
        self.assertTrue(self.testPhoneBook.addRecord(self.user11))
        self.assertTrue(self.testPhoneBook.addRecord(self.user12))
        self.assertFalse(self.testPhoneBook.addRecord(self.user12))  # Record should already be in PhoneBook

        print("Current PhoneBook Size is: ", self.testPhoneBook.entryCount())
        self.assertEqual(self.testPhoneBook.entryCount(), 13)

        # Validate Deletion & Search
        result = list()
        for x in self.testPhoneBook.findRecords('C', 'NAME'):
            result.append(x)

        for U in result:
            self.assertTrue(self.testPhoneBook.deleteRecord(U))
            self.assertFalse(self.testPhoneBook.deleteRecord(U))

        # Validate Record Update Process doesn't create duplicates and the change gets committed
        # 1. Search Before Change, Validate the # of results
        # 2. Make Change, Update Change
        # 3. Search for change, validate result == 1
        # 4  Search for records, validate result == 1
        self.assertTrue(self.testPhoneBook.findRecords('Normal', 'NAME'), 1)
        self.user2_old = self.user2
        self.user2.email('new@xxx.yyy.zzz')
        self.testPhoneBook.updateRecord(self.user2_old, self.user2)
        self.assertTrue(self.testPhoneBook.findRecords('Normal', 'NAME'), 1)
        self.assertTrue(self.testPhoneBook.findRecords('new@xxx.yyy.zzz', 'EMAIL'), 1)

        # Test valid write
        self.assertIsNone(self.testPhoneBook.WritePhoneBook())

# Used for unit testing only
if __name__ == '__main__':
    unittest.main()
