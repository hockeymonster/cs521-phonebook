__author__ = 'msabbota'

from tkinter import *
from tkinter import ttk

from PhoneBook import *
from smsCall import *


'''
root = Tk()
root.title("CS521 PhoneBook Application")
main_window = ttk.Frame(root, padding="10 10 10 10", width=10000, height=10000)
root.mainloop()
'''


class PhoneBookUI(PhoneBook):
    # Initialize the PhoneBook using ./phonebook.dat as the location
    __PB = PhoneBook("./phonebook.dat")

    root = Tk()
    entries = ttk.Treeview()
    statusmessage = StringVar()
    searchcount = 0

    def __init__(self):

        self.root.geometry("800x600+300+300")
        self.root.title("CS521 PhoneBook")
        frame = Frame(self.root, borderwidth=10)

        Grid.rowconfigure(self.root, 0, weight=1)
        Grid.columnconfigure(self.root, 0, weight=1)

        # Input our phonebook view with scrollbar (row=0, column=all)
        # We made this modular so we can easily adjust formatting, columns, etc.
        # bookViewTree always takes a 'search' parameter & constraint, we'll default to null
        self.bookViewTree('.', '')

        # Build the menu/status bar along the bottom (row=1)
        frame.grid(sticky=N + S + E + W, column=0, row=1)
        frame.rowconfigure('all', minsize=200)
        frame.columnconfigure('all', minsize=200)

        # Search
        SearchEntryButton = Button(frame, text="Search", command=self.SearchEntry)
        SearchEntryButton.grid(row=1, column=0)

        # Add Entry Button
        AddEntryButton = Button(frame, text="Add Entry", command=self.addEntry)
        AddEntryButton.grid(row=1, column=1)

        # Modify Entry
        ModifyEntryButton = Button(frame, text="Modify Entry", command=self.modifyEntry)
        ModifyEntryButton.grid(row=1, column=2)

        # Delete Entry
        DELEntryButton = Button(frame, text="Delete Entry", command=lambda: self.delEntry())
        DELEntryButton.grid(row=1, column=3)

        # SMS Button
        SMSEntryButton = Button(frame, text="SMS User", command=lambda: self.smsEntry())
        SMSEntryButton.grid(row=1, column=4)

        # Save Button
        SaveButton = Button(frame, text="Save", command=self.saveEntry)
        SaveButton.grid(row=1, column=5)

        # Quit Button
        QuitButton = Button(frame, text="Exit", command=self.quitPhoneBook)
        QuitButton.grid(row=1, column=6)

        # Message Label
        StatusOutput = Label(frame, textvariable=self.statusmessage, fg="blue")
        StatusOutput.grid(row=1, column=7, padx=20)

        self.root.mainloop()

    def addEntry(self):
        # Allows you to add a new entry to the PhoneBook
        # Precondition: None
        # Postcondition: Status message of addition or notice of failure
        top = Toplevel(self.root)
        top.title("Add PhoneBook Entry")
        top.geometry("350x200+400+400")

        fnlabel = Label(top, text="First Name")
        fnlabel.grid(row=0, column=0, sticky=W)
        fnentry = Entry(top)
        fnentry.grid(row=0, column=1, sticky=E + W, padx=(10, 20))

        lnlabel = Label(top, text="Last Name")
        lnlabel.grid(row=1, column=0, sticky=W)
        lnentry = Entry(top)
        lnentry.grid(row=1, column=1, sticky=E + W, padx=(10, 20))

        elabel = Label(top, text="Email")
        elabel.grid(row=2, column=0, sticky=W)
        einput = Entry(top)
        einput.grid(row=2, column=1, sticky=E + W, padx=(10, 20))

        plabel = Label(top, text="Phone")
        plabel.grid(row=3, column=0, sticky=W)
        pinput = Entry(top)
        pinput.grid(row=3, column=1, sticky=E + W, padx=(10, 20))

        top.rowconfigure(0, weight=0)
        top.columnconfigure(0, weight=0)
        top.columnconfigure(1, weight=2)

        addu = Button(top, text="Add User", command=lambda: addUser(self))
        addu.grid(row=4, column=0, columnspan=2)

        done = Button(top, text="DONE", command=lambda: top.destroy())
        done.grid(row=5, column=0, columnspan=2)

        errormsg = StringVar()
        result = Label(top, textvariable=errormsg, fg="red", font=("bold", 10))
        result.grid(row=6, columnspan=2, pady=5)

        def addUser(self):
            # This is a stub/helper function to assist the 'add-user'.
            # Could have been done with a hard to read lambda statement
            try:
                self.__PB.addRecord(UserEntry(fnentry.get(), lnentry.get(), einput.get(), pinput.get()))
                errormsg.set("User added!")
                self.bookViewTree('.', '')

            except ValueError as e:
                print("Unable to add user: ", str(e))
                errormsg.set("Unable to add user: " + str(e))

    def delEntry(self):
        # Intent: Remove a record from the backend database
        # Precondition: Take a selection from the UI
        # Postcondition: Message to UI console showing deletion
        x = self.entries.item(self.entries.selection())
        (firstName, lastName, email, phone) = x['values']
        self.__PB.deleteRecord(UserEntry(firstName, lastName, email, str(phone)))
        self.statusmessage.set("Deleted user: {0} {1} from the phonebook".format(firstName, lastName))
        self.bookViewTree('.', '')

    def modifyEntry(self):
        # Intent: allow you to edit an existing user
        # Precondition: Selected entry to edit/dialog tab
        # Postcondition: Updates the entry in the phonebook by adding the new one and deleting the old - OR -
        # if items are not changed, the record will not be touched
        x = self.entries.item(self.entries.selection())
        (firstName, lastName, email, phone) = x['values']
        oldrecord = UserEntry(firstName, lastName, email, str(phone))

        top = Toplevel(self.root)
        top.title("Edit PhoneBook Entry")
        top.geometry("350x200+400+400")

        fnlabel = Label(top, text="First Name")
        fnlabel.grid(row=0, column=0, sticky=W)
        fnentry = Entry(top)
        fnentry.insert(0, firstName)
        fnentry.grid(row=0, column=1, sticky=E + W, padx=(10, 20))

        lnlabel = Label(top, text="Last Name")
        lnlabel.grid(row=1, column=0, sticky=W)
        lnentry = Entry(top)
        lnentry.insert(0, lastName)
        lnentry.grid(row=1, column=1, sticky=E + W, padx=(10, 20))

        elabel = Label(top, text="Email")
        elabel.grid(row=2, column=0, sticky=W)
        einput = Entry(top)
        einput.insert(0, email)
        einput.grid(row=2, column=1, sticky=E + W, padx=(10, 20))

        plabel = Label(top, text="Phone")
        plabel.grid(row=3, column=0, sticky=W)
        pinput = Entry(top)
        pinput.insert(0, phone)
        pinput.grid(row=3, column=1, sticky=E + W, padx=(10, 20))

        top.rowconfigure(0, weight=0)
        top.columnconfigure(0, weight=0)
        top.columnconfigure(1, weight=2)

        addu = Button(top, text="Save", command=lambda: saveUser(self))
        addu.grid(row=4, column=0, columnspan=2)

        done = Button(top, text="DONE", command=lambda: top.destroy())
        done.grid(row=5, column=0, columnspan=2)

        errormsg = StringVar()
        result = Label(top, textvariable=errormsg, fg="red", font=("bold", 10))
        result.grid(row=6, columnspan=2, pady=5)

        def saveUser(self):

            # Attempt to save the user, if not present a message to the user
            # Precondition - user data to sanity check and store in the phonebook
            # Postcondition - return to user success -OR- failure and reason for failure
            newrecord = (UserEntry(fnentry.get(), lnentry.get(), einput.get(), pinput.get()))
            if (newrecord == oldrecord):
                errormsg.set("Record not edited!")
                return

            try:
                self.__PB.addRecord(UserEntry(fnentry.get(), lnentry.get(), einput.get(), pinput.get()))
                errormsg.set("User updated!")
                self.__PB.deleteRecord(oldrecord)
                self.bookViewTree('.', '')

            except ValueError as e:
                print("Unable to add user: ", str(e))
                errormsg.set("Unable to add user: " + str(e))

    def SearchEntry(self):
        # Intent: pop up an input box to get search string and search constraint
        # Postcondition: Update viewing pane with updated search results

        top = Toplevel(self.root)
        top.title("Phonebook Search")
        top.geometry("200x125+400+400")
        searchprompt = Label(top, text="Enter search string")
        searchprompt.pack()
        searchentry = Entry(top)
        searchentry.pack()

        constraint = StringVar(top)
        constraint.set("")
        select = OptionMenu(top, constraint, "", "NAME", "EMAIL", "PHONE")
        select.pack()

        searchok = Button(top, text="OK", command=lambda: self.bookViewTree(searchentry.get(), constraint.get()))
        searchok.pack()

    def bookViewTree(self, pattern, constraint):
        # Displays all entries in the phonebook using Treeview()
        # Precondition: filter list & constraint
        # Postcondition: Treeview widget with content containing PhoneBook information

        self.entries = ttk.Treeview()
        self.entries['columns'] = ("fname", "lname", "email", "phone")
        self.entries.grid(row=0, column=0, sticky=N + E + W + S, padx=2, pady=2)

        self.entries["show"] = 'headings'  # Hide the index column
        self.entries.heading("fname", text="First Name", anchor=W)
        self.entries.heading("lname", text="Last Name", anchor=W)
        self.entries.heading("email", text="Email", anchor=W)
        self.entries.heading("phone", text="Phone", anchor=W)

        scroll = Scrollbar(self.root, orient=VERTICAL, command=self.entries.yview)
        scroll.grid(row=0, column=4, sticky=N + S)
        scroll.columnconfigure(4, weight=1)
        scroll.rowconfigure(0, weight=1)
        self.entries.configure(yscrollcommand=scroll.set)

        self.searchcount += 1
        self.statusmessage.set("Search #{0} Complete.".format(self.searchcount))
        print("BookView searching with pattern={0} and constraint={1}".format(pattern, constraint))
        for i, x in enumerate(self.__PB.findRecords(pattern, constraint)):
            self.entries.insert("", 0, i, values=(x.get_firstName(), x.get_lastName(), x.get_email(), x.get_phone()))

    def saveEntry(self):
        # Intent: Save in-memory phonebook to disk
        # UI Button that returns status, all work documented handled by lower-level PhoneBook class
        print("Saving phonebook to disk.")
        self.statusmessage.set("Saving Phonebook to disk.")
        self.__PB.WritePhoneBook()

    def smsEntry(self):
        # Intent: Send a text/sms message to a user
        # Precondition: User with a valid phone number selected
        # Postcondition: Result of success/fail of text sending
        def textUser(phone, data):
            try:
                SendSMS(phone, data)
                result.set("SMS Sent!")
                self.statusmessage.set("Text message sent to {0} {1} at ({2})".format(fname, lname, phone))
            except twilio.TwilioRestException as x:
                result.set("SMS failed to send: {0}".format(str(x)))
                self.statusmessage.set(
                    "Text message sent to {0} {1} at ({2}) failed to send".format(fname, lname, phone))

        top = Toplevel(self.root)
        top.title("SMS User")
        top.geometry("600x250+400+400")

        (fname, lname, email, phone) = self.entries.item(self.entries.selection())['values']

        smsprompt1 = Label(top, text="Send Message to {0} {1} @ ({2})".format(fname, lname, phone))
        smsprompt1.pack()

        smsprompt2 = Label(top, text="Enter message to send:")
        smsprompt2.pack()

        smsdata = Entry(top)
        smsdata.pack()

        sendb = Button(top, text="SEND", command=lambda: textUser(phone, smsdata.get()))
        sendb.pack()

        done = Button(top, text="DONE", command=lambda: top.destroy())
        done.pack()

        result = StringVar(top)
        result.set("")
        Label(top, textvariable=result, fg="blue", font=("bold", 10)).pack()


    def quitPhoneBook(self):
        # User tried to exist, save entries and quit
        self.__PB.WritePhoneBook()
        self.root.destroy()


if __name__ == '__main__':
    pb = PhoneBookUI()
