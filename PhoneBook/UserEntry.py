__author__ = 'msabbota'

from validate import *


class UserEntry(object):
    def __eq__(self, other):
        return (self._firstName, self._lastName, self._email, self._phone) == (
            other._firstName, other._lastName, other._email, other._phone)

    def __ne__(self, other):
        return not self == other

    def __init__(self, firstName, lastName, email, phone):
        """
          Intent: Create a valid userRecord for the phoneBook
          Precondition: Valid first & last Name
          PostCondition 1: Record with first & lastName that passes validName
            OR
            raise Error & return None

          All entries can be updated after the fact

          Default python __setattr__, __getattr__ not used as we want the class to validate constraints

        """

        # Postcondition 1 met
        if validName(firstName) and validName(lastName):
            self._firstName = firstName
            self._lastName = lastName
        else:
            raise ValueError('Invalid name input')

        if validEmail(email):
            self._email = email
        else:
            raise ValueError('Invalid email input')

        if validPhone(phone):
            self._phone = phone
        else:
            raise ValueError('Invalid phone input')

    def get_email(self):
        return self._email

    def email(self, value):
        if validEmail(value):
            self._email = value
        else:
            raise ValueError('Invalid email input')

    def get_firstName(self):
        return self._firstName

    def firstName(self, value):
        if validName(value):
            self._firstName = value
        else:
            raise ValueError('Invalid Name input')

    def get_lastName(self):
        return self._lastName

    def lastName(self, value):
        if validName(value):
            self._lastName = value
        else:
            raise ValueError('Invalid Name input')
        self._lastName = value

    def entry_match(self, regex, constraint):
        # Precondition: a pre-compiled regex - AND - a search constraint for a field to search (can be None which
        # searches all fields)
        # Postcondition: True if the record matches
        if (constraint == 'EMAIL' or constraint == "") and regex.search(self._email): return True
        if (constraint == 'PHONE' or constraint == "") and regex.search(self._phone): return True
        if (constraint == 'NAME' or constraint == "") and regex.search(self._firstName + self._lastName): return True
        return False

    def get_phone(self):
        return self._phone

    def phone(self, value):
        if validPhone(value):
            self._phone = value
        else:
            raise ValueError('Invalid Phone Error')

    def to_string(self):
        return "{0} {1}\t{2}\t{3}".format(self._firstName, self._lastName, self._email, self._phone)

# Used for unit testing only
if __name__ == '__main__':
    pass

