__author__ = 'msabbota'

import unittest

from smsCall import *


class test_SMS(unittest.TestCase):
    def test_sendSMS(self):
        # Test1: Send a valid SMS to a phone number
        # Test2: Try sending to an invalid number
        # self.assertTrue(SendSMS("12482522883", 'Unit Test Message'))
        self.assertRaises(twilio.rest.exceptions.TwilioRestException, lambda: SendSMS("10", 'Unit Test Message'))

# Used for unit testing
if __name__ == '__main__':
    unittest.main()
