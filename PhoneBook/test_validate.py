__author__ = 'msabbota'

import unittest

from UserEntry import *


class ValidateTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_Validate(self):
        # Validate E-mail for syntax and invalid chars
        self.assertTrue(validEmail('x@y.com'))
        self.assertFalse(validEmail('adsfadfadsdasdfsdf'))

        # Validate phone numbers for # only and certain length
        self.assertTrue(validPhone('2482522883'))
        self.assertFalse(validPhone('12311321212312312312'))
        self.assertFalse(validPhone('x12311321212312312312'))

        # Validate Name Input
        self.assertTrue(validName('test'))
        self.assertFalse(validName(''))

# Used for unit testing
if __name__ == '__main__':
    unittest.main()
