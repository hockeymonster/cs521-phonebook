__author__ = 'msabbota'

'''
 This is going to be a class that implements the following functions
 Intent: Stores array of UserEntry records
    Supports the following functions:
    - Search based on Name (First & Last)
    - Search based on Phone Number
    - Search based on E-mail
    - Add an entry
    - Delete an entry
    - Update/Replace an entry

'''

import pickle

from UserEntry import *


class PhoneBook(object):
    __book = list()  # Working items
    constraint = None
    __datafile = None

    def __init__(self, a_datafile):

        self.__datafile = a_datafile
        # Intent: Read any phonebook entries on disk into memory
        self.ReadPhoneBook()

    def addRecord(self, record):
        # Intent: Add a new phone record to the Phonebook
        # Precondition: A valid UserEntry object
        # Postcondition: Return True or False on add success or failure
        if isinstance(record, UserEntry) and not record in self.__book:
            self.__book.append(record)
            return True
        else:
            print("Warning! Record not added: Invalid UserEntry() object or Already exists in the PhoneBook",
                  type(record))
        return False

    def clearRecords(self):
        # Intent: Zero out the phonebook
        # Precondition: None
        # Empties on-disk file & memory
        self.__book = list()
        self.WritePhoneBook()
        return True

    def deleteRecord(self, record):
        # Intent: delete a record out of the database
        # Precondition: phone record index to remove
        # Postcondition: Return True or False on delete success or failure
        try:
            self.__book.remove(record)
        except ValueError as e:
            print("Unable to remove record: ", e)
            return False
        return True

    def entryCount(self):
        # Return numbers of items in the book
        return len(self.__book)

    def bookEnum(self):
        for e in self.__book:
            yield e

    def findRecords(self, pattern, constraint):

        # Intent: return all records that match string & constraint
        # Precondition: Name (string or digits) and any constraint (first, name, email, phone, all)
        # PostCondition: returns list of phone records in the database

        regex = re.compile(pattern, re.IGNORECASE)
        for e in self.__book:
            if e.entry_match(regex, constraint):
                yield e

    def printBook(self):
        print("Printing records in Phonebook!")
        for i, r in enumerate(self.__book):
            print("Record {0} -> {1}".format(i, r.to_string()))

    def updateRecord(self, record_old, record_new):
        # Intent: Update the records of a phone content
        # Precondition: Record to be modified & updated record data
        # Postcondition: Return True or False based on success or failure
        if isinstance(record_new, UserEntry):
            for i, v in enumerate(self.__book):
                if (v == record_old): self.__book[i] = record_new
        else:
            print("Warning! Record not added: Invalid UserEntry() object: ", type(record_new))
            return False
        return True

    def ReadPhoneBook(self):
        # Intent: read saved entries from disk into memory
        # Precondition: __datafile is NOT Null and set to an appropriate location
        # Postcondition: Print success & number of records written -OR- throw an error why

        try:
            with open(self.__datafile, 'rb', buffering=0) as db:
                self.__book = pickle.loads(db.read())
                print("Loaded {0} records from {1}".format(self.entryCount(), self.__datafile))
        except IOError as e:
            print("Unable to read files from disk: ", e)
            pass
        except EOFError as e:
            print("Unable to read files from disk: ", e)
            pass

    def WritePhoneBook(self):
        # Intent: save entries to disk
        # Precondition: __datafile is NOT Null and set to an appropriate location
        # Postcondition: Print success & number of records written -OR- throw an error why

        try:
            with open(self.__datafile, 'wb', buffering=0) as db:
                try:
                    db.write(pickle.dumps(self.__book))
                except IOError as e:
                    print("Unable to save {0} phonebook records to database ({1})".format(self.entryCount(), e))
        except IOError as e:
            print("Unable to save {0} phonebook records to database ({1})".format(self.entryCount(), e))

# Used for unit testing only
if __name__ == '__main__':
    pass

