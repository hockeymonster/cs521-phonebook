__author__ = 'msabbota'

import unittest

from UserEntry import *


class UserEntryTest(unittest.TestCase):
    '''
        Test criteria for UserEntry validates acceptable input into all fields for a UserEntry class
        The following fields are tested for validity
            - FirstName
            - LastName
            - E-mail
            - Phone
    '''

    def setUp(self):
        self.user0 = UserEntry("John", "Doe", "junk@foo.com", "15551212")
        self.user1 = UserEntry("Flash", "Gordon", "flash@flash.net", "17777777")
        self.user2 = UserEntry("Normal", "User", "a@b.net", "222333444")

    def test_namevalues(self):
        self.assertEqual("John Doe", self.user0._firstName + " " + self.user0._lastName)
        self.assertEqual("flash@flash.net", self.user1._email)

        # Test invalid name, phone, email
        self.assertRaises(ValueError, lambda: UserEntry("", "", "foo@bar.net", "5551212"))
        self.assertRaises(ValueError, lambda: UserEntry("A", "B", "-asdfsd.net", "5551212"))
        self.assertRaises(ValueError, lambda: UserEntry("A", "B", "foo@bar.net", "xxaslfascmalw"))

        # Test invalid constructor args
        self.assertRaises(TypeError, lambda: UserEntry("Normal", "User", "a@b.net", "222333444"), 'adsafasf')

        # EMAIL TEST - current, failed update, valid update
        self.assertRaises(ValueError, lambda: self.user2.email('xadsfasdfasdf'))
        self.assertEqual("a@b.net", self.user2._email)
        self.user2.email("new@test.com")
        self.assertEqual("new@test.com", self.user2.get_email())

        # PHONE TEST - current, failed update, valid update
        self.assertRaises(ValueError, lambda: self.user2.phone('xadsfasdfasdf'))
        self.assertEqual("222333444", self.user2.get_phone())
        self.user2.phone("999123123")
        self.assertEqual("999123123", self.user2.get_phone())

        # TEST First & Last Name
        self.assertEqual("Normal", self.user2.get_firstName())
        self.assertRaises(ValueError, lambda: self.user2.firstName(""))
        self.assertRaises(ValueError, lambda: self.user2.lastName(""))

    def test_entryregex(self):
        # ENTRY MATCH TEST
        regex = re.compile("john", re.IGNORECASE)
        self.assertTrue(self.user0.entry_match(regex, constraint=""))
        self.assertFalse(self.user0.entry_match(regex, 'EMAIL'))
        self.assertFalse(self.user0.entry_match(regex, 'PHONE'))

# Used for unit testing only
if __name__ == '__main__':
    unittest.main()