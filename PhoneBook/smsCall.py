'''
   Uses Twilio (twilio.com) to send text/SMS messages. Leverages REST API and appropraite Twilio python module
   Leveraged Twilio Python helper libraries at https://www.twilio.com/docs

   This does use REST and could easily be implemented into it's own class in which I may manually build time permitting
'''

import twilio
import twilio.rest

def SendSMS(sms_dest, text):
    # Precondition: A valid phone number AND text-message body
    # Postcondition: Either successful send -OR- throw an error

    twilio_id = 'ACa6bb1a2950472c8667f38bb976326617'
    twilio_auth = '4de918679e1af05238fc9e581588168c'
    twilio_sender = '+1(248)-513-8439'

    try:
        smsCall = twilio.rest.TwilioRestClient(twilio_id, twilio_auth)
        result = smsCall.messages.create(body=text, to=sms_dest, from_=twilio_sender)
        return result
    except twilio.TwilioRestException as x:
        raise x

# Used for unit testing
if __name__ == '__main__':
    pass
